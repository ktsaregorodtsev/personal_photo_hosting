<?php

global $mainConnection;
require('./engine/databaseconnection.php');

class ScriptBlock {
    private $name;
    private $group;
    private $cheldren;
    private $lines = [];
    private $current_line;
    private $endkey = null;

    public function __get($property) {
        switch ($property) {
            case 'name':
                return $this->name;
            case 'group':
                return $this->group;
            case 'lines':
                return $this->lines;
            default:
                break;
        }
    }

    public function __set($property, $value) {
        switch ($property) {
            case 'name':
                $this->name = value;
                break;
            case 'group':
                $this->name = value;
                break;
            default:
                break;
        }
    }

    public function addLine($line) {
        $line = trim($line);
        // TODO Выполнение поиска
        # Функция должна определять строки, разделенных точкой с запятой
        if(strpos($line, $this->endkey) != false) return true;
        if(substr($line, 0, 2) == '--') {
            $command = ltrim(substr($line, 2));
            if(substr($command, 0, 7)  == '@module') {
                $this->endkey = "@endmodule";
                $this->group = "module";
                $name = ltrim(substr($command, 7));
                preg_match('/\w+/', $name, $matches);
                $this->name = $matches[0] ?? null;
            }
            if(substr($command, 0, 5)  == '@test') {
                $this->endkey = "@endtest";
                $this->group = "test";
                $name = ltrim(substr($command, 5));
                preg_match('/\w+/', $name, $matches);
                $this->name = $matches[0] ?? null;
            }
        }
        else {
            $commads = explode(';', $line);
            $this->current_line .= ($commads[0] ?? '').' ';
            if(count($commads) == 2) {
                array_push($this->lines, $this->current_line);
                $this->current_line = $commads[1];
            }
        }
        return false;
    }
};

class ScriptParcer {
    private $directory = './sqlscripts';
    private $blocks = [];

    public function __construct() {
        foreach (glob("$this->directory/*.sql") as $key => $value) {
            $this->parceFile($value);
        }
        $this->runScript();
    }

    private function parceFile($filename) {
        $filahandle = fopen($filename, 'r');
        $line = '';
        $block = new ScriptBlock();
        while($fileline = fgets($filahandle)) {
            if($block->addLine($fileline)) {
                array_push($this->blocks, $block);
                $block = new ScriptBlock();
            }
        }
        fclose($filahandle);
    }

    private function runScript() {
        global $mainConnection;
        foreach ($this->blocks as $val) {
            print($val->group.':'.$val->name.' started'.PHP_EOL);
            foreach ($val->lines as $key => $line) {
                $status = $mainConnection()->exec($line);
                $text = $status !== false ? '[OK]' : '[FAIL]';
                print($key.':'.$text.PHP_EOL);
                if($status === false){
                    $errorInfo = $mainConnection()->errorInfo();
                    print('Error code: '.$errorInfo[0].':'.$errorInfo[2].PHP_EOL);
                }
            }
            print($val->group.':'.$val->name.' finished'.PHP_EOL);
        }
    }
};

/**
 * @brief Запуск команды
 */
function run_command($arguments) {
    new ScriptParcer();
    return true;
};
/**
 * @brief Функция выводит справку по команде
 * @node Результат работы функции -- массив, состоящий из
 * следующих элементов:
 * - description -- описание команды
 * - parametrs -- параметры командной строки в виде [0=> array['command' => '--comand', 'short' => '-C', 'description' => 'Описание']]
 * - fullhelp -- выводит полную справку по команде полная справка.
 */
 function help() {
     $no_test = [
         'command' => '--notest',
         'short' => '-T',
         'description' => 'Не проводить интеграционные тесты.'
      ];
      return [
          'name' => 'initial',
          'description' => "Комада для работы с базой данных. Разворачивает базу данных из директории sqlscripts",
          'parametrs' => [0 => $no_test],
          'fullhelp' => 'Скоро будем...'
      ];
}
?>
