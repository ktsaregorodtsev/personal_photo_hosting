<?php

$require_prefix = $require_prefix ?? './';

function admin_modules() {
    $items = [];
    foreach (glob('./admin/*.php') as $key => $value) {
        array_push($items, basename($value, '.php'));
    }
    return $items;
}

function printHelp($command = null){
    if(!$command) {
        echo "Инструмент администрирования. Использование:".PHP_EOL;
        echo "php admin_tool.php <команда>".PHP_EOL;
        echo "\thelp\t\t\tВыести справку".PHP_EOL;
        echo "\thelp <имя модуля>\tВыести полную справку по модулю".PHP_EOL;
        echo "\tДоступные модули: ".PHP_EOL;
        foreach (admin_modules() as $key => $value)
            echo "\t\t$value".PHP_EOL;
    }
    else {
        # TODO try catch
        echo $command["name"].' '.$command["description"].PHP_EOL;
        if($command["parametrs"]) {
            echo "Доступные параметры:".PHP_EOL;
            foreach ($command["parametrs"] as $key => $value){
                echo "\t".$value["short"].", ".$value["command"]."\t".$value["description"].PHP_EOL;
            }
        }
    }
}

function loadModule($module) {
    require "./admin/$module.php";
}

function main() {
    global $argv;
    $modules = admin_modules();
    $command = $argv[1] ?? '';
    $args = array_slice($argv, 2);
    $help_commands = ['-h', '--help', 'help'];
    if(in_array($command, $help_commands)){
        if(count($args) > 0 && in_array($args[0], $modules)) {
            loadModule($args[0]);
            printHelp(help());
        }
        else {
            printHelp();
        }
        return;
    }
    if(in_array($command, $modules)) {
        loadModule($command);
        run_command($args);
        return;
    }
    echo "Не правильное использование команды.".PHP_EOL.PHP_EOL;
    printHelp();
}

main();
?>
