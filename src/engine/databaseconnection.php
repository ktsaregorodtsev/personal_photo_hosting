<?php

global $require_prefix;
global $settings;
require $require_prefix.'/settings.php';


class DatabaseConnection
{
    private $_connection = NULL;

    public function __construct() {
        global $settings;
        if($settings["db"]["driver"] == 'sqlite') {
            $this->_connection = new PDO('sqlite:'.$settings["db"]["dbname"]);
        }
        else {
            $driver   = $settings["db"]["driver"];
            $host     = $settings["db"]["host"];
            $dbname   = $settings["db"]["dbname"];
            $charset  = $settings["db"]["charset"];
            $dsn      = "$driver:host=$host;dbname=$dbname;charset=$charset";
            $this->_connection = new PDO(
                $dsn, $settings["db"]["user"],
                $settings["db"]["pass"],
                $settings["db"]["opt"]);
        }
    }

    public function &__invoke() {
        return $this->_connection;
    }
}
// TODO Синглтон
$mainConnection = new DatabaseConnection();

 ?>
