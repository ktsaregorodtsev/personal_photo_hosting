<?php

$settings = array();

$production = [
    "driver"    => 'mysql',
    "host"      => 'localhost',
    "dbname"    => 'databasename',
    "user"      => 'user_name',
    "password"  => 'passwd',
    "charset"   => 'utf8'];

$production["opt"] = [
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_SILENT,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES   => false];

$develop = [
    "driver"    => 'mysql',
    "host"      => 'localhost',
    "dbname"    => 'databasename',
    "user"      => 'user_name',
    "password"  => 'passwd',
    "charset"   => 'utf8'];

$develop["opt"] = [
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_WARNING,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES   => false];

$test = [
    "driver"    => 'sqlite',
    "dbname"    => '/tmp/testdatabase.sqlite'];

$test["opt"] = [
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES   => false];

$settings['db'] = $test;
global $settings;

?>
