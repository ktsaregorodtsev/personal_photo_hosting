-- @module users --
CREATE TABLE IF NOT EXISTS t_user(
    user_uuid VARCHAR(32) PRIMARY KEY,
    user_name VARCHAR(100) NOT NULL,
    user_first_name VARCHAR(100) NOT NULL  DEFAULT '',
    user_middle_name VARCHAR(100) NOT NULL DEFAULT '',
    user_last_name VARCHAR(100) NOT NULL  DEFAULT '',
    password_summ VARCHAR(32) NOT NULL DEFAULT '',
    join_timestamp TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    blocked INT(1),
    expire_timestamp TIMESTAMP,
    last_login TIMESTAMP,
    UNIQUE KEY (user_name)
);

CREATE TRIGGER before_insert_user BEFORE INSERT ON t_user
  FOR EACH ROW  SET new.user_uuid = replace(uuid(), '-', '');

CREATE TABLE IF NOT EXISTS t_session(
    session_uuid VARCHAR(32) PRIMARY KEY,
    user_uuid VARCHAR(32) REFERENCES t_user(user_uuid),
    created_timestamp TIMESTAMP NOT NULL,
    expire_timestamp TIMESTAMP NOT NULL,
    session_data BLOB NOT NULL
);

CREATE TRIGGER before_insert_session BEFORE INSERT ON t_session
  FOR EACH ROW  SET new.session_uuid = replace(uuid(), '-', ''),
                    new.created_timestamp = now(),
                    new.expire_timestamp = date_add(now(), interval 10 minute);

CREATE TABLE t_social(
    t_social_id INT AUTO_INCREMENT PRIMARY KEY,
    user_uuid VARCHAR(32) REFERENCES t_user(user_uuid),
    social_name VARCHAR(100) NOT NULL,
    social_url VARCHAR(100) NOT NULL,
    social_token VARCHAR(100) NOT NULL
);
-- @endmodule --

-- @test users --
INSERT INTO `t_user`(`user_name`, `password_summ`) VALUES ('login', MD5('1234'));
INSERT INTO `t_session`(`session_data`) VALUES ('{}');
-- @endtest --
